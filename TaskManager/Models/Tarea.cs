﻿using System.ComponentModel.DataAnnotations;

namespace TaskManager.Models
{
    public class Tarea
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string? Titulo { get; set; }
        [Required]
        public string? Descripcion { get; set; }
    }
}
