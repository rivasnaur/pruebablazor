﻿using Microsoft.EntityFrameworkCore;
using System.Net.Http.Headers;
using TaskManager.Models;

namespace TaskManager.Data
{
    public class TareaService 
    {

        private readonly IDbContextFactory<AppDbContext> _dbContextFactory;

        public TareaService (IDbContextFactory<AppDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<IEnumerable<Tarea>> GetAllTareasAsync()
        {
            using var context = await _dbContextFactory.CreateDbContextAsync();
            return await context.Tareas.ToListAsync();
        }

        public async Task<Tarea?> GetTareaByIdAsync(int id)
        {
            using var context = await _dbContextFactory.CreateDbContextAsync();
            return await context.Tareas.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Tarea?> UpdateTareaAsync(Tarea updatedTarea)
        {
            using var context = await _dbContextFactory.CreateDbContextAsync();
            var existingTarea = await context.Tareas.FirstOrDefaultAsync(p => p.Id == updatedTarea.Id);
            if (existingTarea != null)
            {
                existingTarea.Titulo = updatedTarea.Titulo;
                existingTarea.Descripcion = updatedTarea.Descripcion;
                await context.SaveChangesAsync();
                return existingTarea;
            }
            return null;
        }

        public async Task DeleteTareaAsync(int id)
        {
            using var context = await _dbContextFactory.CreateDbContextAsync();
            var existingTarea = await context.Tareas.FirstOrDefaultAsync(p => p.Id == id);
            if (existingTarea != null)
            {
               context.Tareas.Remove(existingTarea);
                await context.SaveChangesAsync();
            }          
        }

        public async Task<Tarea> AddTareaAsync(Tarea tarea)
        {
            using var context = await _dbContextFactory.CreateDbContextAsync();
            context.Tareas.Add(tarea);
            await context.SaveChangesAsync();
            return tarea;

        }


        //private readonly TareaContext _context;

        //public TareaService(TareaContext context)
        //{
        //    _context = context;
        //}

        //public async Task<bool> DeleteTareaById(int id)
        //{
        //    var tarea = await _context.Tareas.FindAsync(id);

        //    _context.Tareas.Remove(tarea);
        //    return await _context.SaveChangesAsync() > 0;
        //}

        //public async Task<IEnumerable<Tarea>> GetAllTarea()
        //{
        //    return await _context.Tareas.ToListAsync();
        //}

        //public async Task<Tarea> GetTareaById(int id)
        //{
        //    return await _context.Tareas.FindAsync(id);
        //}

        //public async Task<bool> InsertTarea(Tarea tarea)
        //{
        //    _context.Tareas.Add(tarea);

        //    return await _context.SaveChangesAsync() > 0;
        //}

        //public async Task<bool> SaveTarea(Tarea tarea)
        //{
        //    if (tarea.Id > 0)
        //        return await UpdateTarea(tarea);
        //    else
        //        return await InsertTarea(tarea);
        //}

        //public async Task<bool> UpdateTarea(Tarea tarea)
        //{
        //    _context.Entry(tarea).State = EntityState.Modified;

        //    return await _context.SaveChangesAsync() > 0;
        //}

    }
}
